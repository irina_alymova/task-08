package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.util.ListSorters;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.ArrayList;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		List<Flower> flowers1 = new ArrayList<>();

		// sort (case 1)
		ListSorters.sorterByName(flowers1);
		
		// save
		String outputXmlFile = "output.dom.xml";

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		SAXParserFactory factory = SAXParserFactory.newInstance();
		ArrayList<Flower> flowers2 = new ArrayList<>();
		try {
			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(xmlFileName, saxController);
		} catch (ParserConfigurationException | SAXException e) {
			e.printStackTrace();
		}

		// sort  (case 2)
		ListSorters.sorterByOrigin(flowers2);
		
		// save
		outputXmlFile = "output.sax.xml";

		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
	}

}

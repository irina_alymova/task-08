package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.controller.util.XMLStringConstants;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.Soil;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import static com.epam.rd.java.basic.task8.controller.util.XMLStringConstants.*;

import java.util.ArrayList;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

}
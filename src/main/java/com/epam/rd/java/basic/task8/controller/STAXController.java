package com.epam.rd.java.basic.task8.controller;

import org.xml.sax.helpers.DefaultHandler;

import static com.epam.rd.java.basic.task8.controller.util.XMLStringConstants.*;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

}
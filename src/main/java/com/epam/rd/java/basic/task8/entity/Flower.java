package com.epam.rd.java.basic.task8.entity;

public class Flower {
        String name;
        Soil soil;
        String origin;
        VisualParameters visualParameters;
        GrowingTips growingTips;
        String multiplying;

    public Flower(String name, Soil soil, String origin, VisualParameters visualParameters, GrowingTips growingTips, String multiplying) {
        this.name = name;
        this.origin = origin;
        this.multiplying = multiplying;
    }

    public String getName() {
        return name;
    }

    public Soil getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public String getMultiplying() {
        return multiplying;
    }
}



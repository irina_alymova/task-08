package com.epam.rd.java.basic.task8.entity;

public class GrowingTips {
    private MeasuredValue temperature;
    private Lighting lighting;
    private MeasuredValue wateringMeasure;

    public GrowingTips(MeasuredValue temperature, Lighting lighting, MeasuredValue wateringMeasure) {
        this.temperature = temperature;
        this.lighting = lighting;
        this.wateringMeasure = wateringMeasure;
    }

    public MeasuredValue getTemperature() {
        return temperature;
    }

    public Lighting getLightRequiring() {
        return lighting;
    }

    public MeasuredValue getWateringMeasure() {
        return wateringMeasure;
    }
}

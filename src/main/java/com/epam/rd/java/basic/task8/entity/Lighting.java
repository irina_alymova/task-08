package com.epam.rd.java.basic.task8.entity;

public class Lighting {
    private String lightRequiring;

    public Lighting(String lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    public String getLightRequiring() {
        return lightRequiring;
    }
}

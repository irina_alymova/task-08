package com.epam.rd.java.basic.task8.entity;

public class MeasuredValue {
    private String measure;
    private int value;

    public MeasuredValue(String measure, int value) {
        this.measure = measure;
        this.value = value;
    }

    public String getMeasure() {
        return measure;
    }

    public int getValue() {
        return value;
    }
}

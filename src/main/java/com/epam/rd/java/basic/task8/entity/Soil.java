package com.epam.rd.java.basic.task8.entity;

public enum Soil {
    PODZOLISTAYA("подзолистая"),
    GRUNTOVAYA("грунтовая"),
    DERNOVO_PODZOLYSTAYA("дерново-подзолистая");

    private final String soilName;

    Soil(String soilName) {
        this.soilName = soilName;
    }

    public String getSoilName() {
        return soilName;
    }
}
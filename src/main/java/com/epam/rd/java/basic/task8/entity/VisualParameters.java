package com.epam.rd.java.basic.task8.entity;

public class VisualParameters {
    private String stemColor;
    private String leafColor;
    private MeasuredValue aveLenFlower;

    public VisualParameters(String stemColor, String leafColor, MeasuredValue aveLenFlower) {
        this.stemColor = stemColor;
        this.leafColor = leafColor;
        this.aveLenFlower = aveLenFlower;
    }

    public String getStemColor() {
        return stemColor;
    }

    public String getLeafColor() {
        return leafColor;
    }

    public MeasuredValue getAveLenFlower() {
        return aveLenFlower;
    }
}

package com.epam.rd.java.basic.task8.util;

import com.epam.rd.java.basic.task8.entity.Flower;

import java.util.Comparator;
import java.util.List;

public class ListSorters {

    public static void sorterByName(List<Flower> list) {
        list.sort(Comparator.comparing(Flower::getName));
    }

    public static void sorterByOrigin(List<Flower> list) {
        list.sort(Comparator.comparing(Flower::getOrigin));
    }

    public static void sorterByMultiplying(List<Flower> list) {
        list.sort(Comparator.comparing(Flower::getMultiplying));
    }
}
